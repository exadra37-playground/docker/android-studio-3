# ANDROID STUDIO 3 DOCKER STACK

The Android Studio 3 with the devices emulator running from inside a Docker Container.


## 1. How to Install

```
git clone git@gitlab.com:exadra37-playground/docker/android-studio-3.git && cd android-studio-3
```

Building the Docker image:

```bash
./studio build
```


## 2. How to Use

The code for your app project needs to go inside the folder `./app`.

### 2.1 Starting the editor

```
./studio
```

### 2.2 Stops the editor

Simply close the editor.

Alternatively:

```bash
./studio stop
```

Any changes done to files in the project or to the editor will persist once
everything is being persisted in the host, not in the container.


### 2.3 Shell for the Docker Container Running the Editor

For debug purposes related with the Editor or to `apt-get install` is necessary to have the Editor already open, like explained in 2.1.

```bash
./studio shell
```

If the editor is not open than the above command will start the container and
any changes made to it will be lost when exiting it.

When the editor is open any changes done when inside the container shell will
persist after exiting the container shell or close the editor.
