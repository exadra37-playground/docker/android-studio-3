FROM ubuntu:17.10

# Inspired on:
#   → https://github.com/Purik/android-studio-docker
#   → https://github.com/Deadolus/android-studio-docker

# Removes DBUS warning that should be only seen by Developer
# https://bugs.launchpad.net/ubuntu/+source/at-spi2-core/+bug/1193236
ENV NO_AT_BRIDGE=1

# Will not prompt for questions
ENV DEBIAN_FRONTEND=noninteractive

ARG CONTAINER_USER="${CONTAINER_USER:-android}"
ARG CONTAINER_UID="${CONTAINER_UID:-1000}"

RUN dpkg --add-architecture i386 && \
    apt-get update && \
    apt-get -y upgrade && \
    apt-get -y install \
      locales \
      tzdata \
      ca-certificates \
      apt-utils \
      inotify-tools \
      curl \
      git \
      zsh \
      unzip \
      xorg \
      dbus* \
      nano \
      ant \
      libcanberra-gtk-module \
      firefox \
      default-jdk \
      libz1 \
      libncurses5 \
      #libbz2-1.0:i386 \
      libstdc++6 \
      libbz2-1.0 \
      lib32stdc++6 \
      #lib32z1 \

      build-essential \
      sudo \
      wget \
      libc6:i386 libncurses5:i386 libstdc++6:i386 lib32z1 libbz2-1.0:i386 \
      libxrender1 libxtst6 libxi6 libfreetype6 libxft2 \
      kvm qemu qemu-kvm libvirt-bin ubuntu-vm-builder bridge-utils libnotify4 libglu1 libqt5widgets5 xvfb && \

    locale-gen en_GB.UTF-8 && \
    dpkg-reconfigure locales && \

    bash -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)" && \

    useradd -m -u ${CONTAINER_UID} -s /usr/bin/zsh ${CONTAINER_USER} && \

    su "${CONTAINER_USER}" -c 'bash -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"' && \

    adduser ${CONTAINER_USER} libvirt && \
    adduser ${CONTAINER_USER} kvm && \

    echo "${CONTAINER_USER}:${CONTAINER_USER}" | chpasswd && \
    echo "${CONTAINER_USER} ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/90-${CONTAINER_USER}  && \
    usermod -aG sudo $CONTAINER_USER && \
    usermod -aG plugdev $CONTAINER_USER && \

    curl 'https://dl.google.com/dl/android/studio/ide-zips/3.1.2.0/android-studio-ide-173.4720617-linux.zip' > /tmp/studio.zip && \
    unzip -d /opt /tmp/studio.zip && \
    rm /tmp/studio.zip && \

    apt-get -y -f install && \
    apt-get clean && \
    apt-get purge

ENV LANG en_GB.UTF-8
ENV LANGUAGE en_GB:en
ENV LC_ALL en_GB.UTF-8
ENV ANDROID_EMULATOR_USE_SYSTEM_LIBS=1

USER ${CONTAINER_USER}

WORKDIR /home/${CONTAINER_USER}/AndroidStudioProjects

CMD ["/opt/android-studio/bin/studio.sh"]
