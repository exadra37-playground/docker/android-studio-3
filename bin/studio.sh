#!/bin/bash

args=/opt/android-studio/bin/studio.sh
container_name=android_studio_3

HOST_ANDROID_STUDIO_DIR=~/.docker-android-studio
HOST_ANDROID_STUDIO_DIR_ANDROID=${HOST_ANDROID_STUDIO_DIR}/Android
HOST_ANDROID_STUDIO_DIR_HIDDEN_ANDROID=${HOST_ANDROID_STUDIO_DIR}/.android
HOST_ANDROID_STUDIO_DIR_VERSION=${HOST_ANDROID_STUDIO_DIR}/.AndroidStudio3.1
HOST_ANDROID_STUDIO_DIR_GRADLE=${HOST_ANDROID_STUDIO_DIR}/.gradle
HOST_ANDROID_STUDIO_DIR_JAVA=${HOST_ANDROID_STUDIO_DIR}/.java
mkdir -p \
  ${HOST_ANDROID_STUDIO_DIR_ANDROID} \
  ${HOST_ANDROID_STUDIO_DIR_HIDDEN_ANDROID} \
  ${HOST_ANDROID_STUDIO_DIR_VERSION} \
  ${HOST_ANDROID_STUDIO_DIR_GRADLE} \
  ${HOST_ANDROID_STUDIO_DIR_JAVA}

function Run_Android_Studio()
{
  local _args="${1}"
  local _container_name="${2}"

  sudo docker run \
    -it \
    --name="${_container_name}" \
    --net=host \
    --privileged \
    --group-add plugdev \
    --device /dev/dri \
    --volume=/dev/shm:/dev/shm \
    --volume=${HOST_ANDROID_STUDIO_DIR_ANDROID}:/home/android/Android \
    --volume=${HOST_ANDROID_STUDIO_DIR_VERSION}:/home/android/.AndroidStudio3.1 \
    --volume=${HOST_ANDROID_STUDIO_DIR_HIDDEN_ANDROID}:/home/android/.android \
    --volume=${HOST_ANDROID_STUDIO_DIR_GRADLE}:/home/android/.gradle \
    --volume=${HOST_ANDROID_STUDIO_DIR_JAVA}:/home/android/.java \
    --volume=$PWD/app:/home/android/AndroidStudioProjects \
    --volume="${XSOCK}":"${XSOCK}":ro \
    --volume="${XAUTH}":"${XAUTH}":ro \
    --env="XAUTHORITY=${XAUTH}" \
    --env="DISPLAY" \
    exadra37/android-studio "${_args}"
}

function Build_Docker_Image()
{
  sudo docker build -t exadra37/android-studio ./docker/build/android-studio/3/
}

# Setup X11 server authentication
# @link http://wiki.ros.org/docker/Tutorials/GUI#The_isolated_way
XSOCK=/tmp/.X11-unix
XAUTH=${HOST_ANDROID_STUDIO_DIR}/.docker.xauth
touch "${XAUTH}"
xauth nlist "${DISPLAY}" | sed -e 's/^..../ffff/' | xauth -f "${XAUTH}" nmerge -

if [ "${1}" == "build" ]
  then
    Build_Docker_Image
    exit 0
fi

if [ "${1}" == "stop" ]
  then
    sudo docker container stop "${container_name}"
    exit 0
fi

if [ "${1}" == "shell" ] && sudo docker container ls | grep -q android_studio_3 -
  then
    sudo docker container exec -it "${container_name}" zsh
    exit 0
fi

if [ "${1}" == "shell" ] && sudo docker container ls -a | grep -q android_studio_3 -
  then
    #printf "\n---> The container is stopped. Please start it first with: ./studio <---\n"
    Run_Android_Studio "zsh" "${container_name}_shell"
    sudo docker container rm "${container_name}_shell"
    exit 0
fi

if [ "${1}" == "shell" ]
  then
    Run_Android_Studio "zsh" "${container_name}"
    sudo docker container rm "${container_name}"
    exit 0
fi

if sudo docker container ls -a | grep -q android_studio_3 -
  then
    sudo docker container start android_studio_3
    exit 0
fi

Run_Android_Studio "${args}" "${container_name}"
